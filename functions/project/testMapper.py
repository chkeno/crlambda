import unittest
import json
from Mapper import Mapper

import matplotlib.pyplot as plt


class TestMapper(unittest.TestCase):
    def test_fillMaps(self):
        m = Mapper(True)
        f = open('testData.json', 'r')
        data = json.load(f)
        f.close()
        m.fillMaps(data)
        testMap = {(-89.378016, 34.245065): 'https://northmiss.craigslist.org/',
                   (-90.1848103, 32.2987573): 'https://jackson.craigslist.org/'}
        self.assertEqual(m.coordsToCities, testMap)

    def test_lineAndPointDistance(self):
        m = Mapper(True)
        self.assertEqual(m.lineAndPointDistance((0, 0), (0, 5), (1, 1)), 1)

    def test_getBounds(self):
        m = Mapper(True)
        result = m.getBounds((0, 0), (5, 5), -1, 2)

    def test_getURLsToSearch(self):
        m = Mapper(False)
        print(m.getURLsToSearch("seattle, wa", "denver, co", 60))


if __name__ == '__main__':
    unittest.main()
