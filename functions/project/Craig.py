import requests
from bs4 import BeautifulSoup
import asyncio
import aiohttp

SEARCH_TEMPLATE = "{}search/sss?query={}&sort=rel"

def search(urls, query):

	formatted_urls = [SEARCH_TEMPLATE.format(url, query) for url in urls]
	async def fetch(session, url):
		    with aiohttp.Timeout(10):
		        async with session.get(url) as response:
		            return await response.text()
		
	async def fetch_all(session, urls, loop):
	    results = await asyncio.gather(
	        *[fetch(session, url) for url in urls],
	        return_exceptions=True  # default is false, that would raise
	    )
	    return results

	loop = asyncio.get_event_loop()
	with aiohttp.ClientSession(loop=loop) as session:
		the_results = loop.run_until_complete(
			fetch_all(session, formatted_urls, loop))

	listings = []

	for result in the_results:
		doc = BeautifulSoup(result, "html.parser")
		rows = doc.find_all('li', class_="result-row")
		
		for row in rows:
			data = {}
			data['link'] = row.find('a')['href']
			if row.find('a', attrs={'class': 'result-image gallery'}):
				data['image'] = 'https://images.craigslist.org/' + row.find('a', attrs={'class': 'result-image gallery'})['data-ids'].split(',')[0].split(':')[1] + '_300x300.jpg'
			else:
				data['images'] = ''
			data['title'] = row.find('a', attrs={'class': 'result-title hdrlnk'}).contents[0]
			
			if row.find('span', attrs={'class':'result-price'}):
				data['price'] = row.find('span', attrs={'class':'result-price'}).contents[0]
			else: 
				data['price'] = ''
			listings.append(data)
	return listings
