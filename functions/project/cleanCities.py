import requests
import json

f = open("allCities.txt")
# g = open("cleanedCities.txt", "w")
data = {}
state = ""
cities = {}
for line in f:
	if "<h4>" in line:
		print(state)
		#store all previous lines
		data[state] = cities

		#
		cities = {}
		state = line.split(">")[1].split("<")[0]

	if "https" in line:
		url = line.split('\"')[1]
		city = line.split('\"')[2].split(">")[1].split("<")[0]
		
		response = requests.get("https://maps.googleapis.com/maps/api/geocode/json?address={},{}&key=AIzaSyC2-9S6HpJ2FjVBmIjKCytyh9fg47RZ_us".format(city, state))
		
		lat = response.json()["results"][0]["geometry"]["location"]["lat"]
		lng = response.json()["results"][0]["geometry"]["location"]["lng"]
		
		cityBody = {"url" : url, "lat" : lat, "lng" : lng}
		
		cities[city] = cityBody

print(state)
data[state] = cities

with open('crData.json', 'w') as fp:
	json.dump(data, fp, indent=4)

fp.close()
f.close()
# g.close()
