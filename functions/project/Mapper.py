import json
import polyline
import requests
import math
import asyncio
import aiohttp


KEY = ""
DISTANCE_API_TEMPLATE = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins={}&destinations={}&key={}"
GEOCODER_API_TEMPLATE = "https://maps.googleapis.com/maps/api/geocode/json?address={}&key={}"
DIRECTIONS_API_TEMPLATE = "https://maps.googleapis.com/maps/api/directions/json?origin={}&destination={}&key={}"
DIRECTIONS_WITH_WAYPOINT_API_TEMPLATE = "https://maps.googleapis.com/maps/api/directions/json?origin={}&destination={}&waypoints=via:{}&key={}"
degreesPerMin = 0.01787

# lng = x
# lat = y
class Mapper:
	coordsToCities = {}
	lats = []
	lngs = []

	def __init__(self, istest=False):
		if not istest:
			f = open('crData.json', 'r')
			data = json.load(f)
			self.fillMaps(data)
			f.close()

	def fillMaps(self, data):
		for state in data:
			cities = data[state]
			for city in cities:
				lat = cities[city]["lat"]
				lng = cities[city]["lng"]
				url = cities[city]["url"]
				self.lats.append(lat)
				self.lngs.append(lng)
				self.coordsToCities[(lng, lat)] = url

	def getURLsToSearch(self, origin, destination, max_deviation_time):

		originLngLat = self.geoCode(origin)
		destLngLat = self.geoCode(destination)


		directions = requests.get(DIRECTIONS_API_TEMPLATE.format(origin, destination, KEY))
		points = polyline.decode(directions.json()["routes"][0]["overview_polyline"]["points"])
		driving_time_minutes = directions.json()["routes"][0]["legs"][0]["duration"]["value"]/60

		coords_lng = []
		coords_lat = []
		flippedPoints = []
		for point in points:
			flippedPoints.append((point[1],point[0]))

		max_dist = 0
		min_dist = 180

		for point in flippedPoints:
			dist = self.lineAndPointDistance(originLngLat, destLngLat, point)
			max_dist = max(max_dist, dist)
			min_dist = min(min_dist, dist)

		deviationDistance = max_deviation_time * degreesPerMin
		origin_max, origin_min, dest_min, dest_max = self.getBounds(originLngLat, destLngLat, min_dist - deviationDistance, max_dist + deviationDistance)

		coordsWithinBounds = []
		for coordinate in self.coordsToCities.keys():
			if self.withinBounds(origin_max, origin_min, dest_max, dest_min, coordinate):
				coordsWithinBounds.append(coordinate)
		
		return self.getDrivingTime(coordsWithinBounds, originLngLat, destLngLat, max_deviation_time)

	def lineAndPointDistance(self, originLngLat, destLngLat, point):
		distance = ((destLngLat[1] - originLngLat[1]) * point[0] - (destLngLat[0] - originLngLat[0]) * point[1] +
					destLngLat[0] * originLngLat[1] - destLngLat[1] * originLngLat[0]) / math.sqrt(
			math.pow(destLngLat[1] - originLngLat[1], 2) + math.pow(destLngLat[0] - originLngLat[0], 2))
		return distance

	def geoCode(self, location):
		response = requests.get(GEOCODER_API_TEMPLATE.format(location, KEY))
		lat = response.json()["results"][0]["geometry"]["location"]["lat"]
		lng = response.json()["results"][0]["geometry"]["location"]["lng"]

		return (lng, lat)

	def withinBounds(self, origin_max, origin_min, dest_max, dest_min, point):
		bound_length = round(abs(self.lineAndPointDistance(origin_min, origin_max, dest_min)), 5)
		bound_width = round(abs(self.lineAndPointDistance(origin_min, dest_min, origin_max)), 5)
		point_length = round(abs(self.lineAndPointDistance(origin_max, origin_min, point)) + abs(self.lineAndPointDistance(dest_max, dest_min, point)), 5)
		point_width = round(abs(self.lineAndPointDistance(origin_max, dest_max, point)) + abs(self.lineAndPointDistance(origin_min, dest_min, point)), 5)
		if point_length == bound_length and point_width == bound_width:
			return True
		else:
			return False

	def getDrivingTime(self,coordsWithinBounds, originLngLat, destLngLat, max_deviation_time):
		urls = [DIRECTIONS_API_TEMPLATE.format(str(originLngLat[1]) + "," + str(originLngLat[0]), str(destLngLat[1]) + "," + str(destLngLat[0]), KEY)]
		for waypointLngLat in coordsWithinBounds:
			originCoords = str(originLngLat[1])+","+str(originLngLat[0])
			destCoords = str(destLngLat[1])+","+str(destLngLat[0])
			waypointCoords = str(waypointLngLat[1])+","+str(waypointLngLat[0])
			urls.append(DIRECTIONS_WITH_WAYPOINT_API_TEMPLATE.format(originCoords, destCoords, waypointCoords, KEY))

		async def fetch(session, url):
		    with aiohttp.Timeout(10):
		        async with session.get(url) as response:
		            return await response.json()
		
		async def fetch_all(session, urls, loop):
		    results = await asyncio.gather(
		        *[fetch(session, url) for url in urls],
		        return_exceptions=True  # default is false, that would raise
		    )
		    return results

		loop = asyncio.get_event_loop()
		with aiohttp.ClientSession(loop=loop) as session:
			the_results = loop.run_until_complete(
				fetch_all(session, urls, loop))

		intialDirections = the_results[0]
		# print(intialDirections)
		if intialDirections["status"] == "OK":
			initial_driving_time = intialDirections["routes"][0]["legs"][0]["duration"]["value"]/60
		else:
			return []

		urlsToSearch = {}
		for i in range(1, len(the_results)):
			directions = the_results[i]
			if directions["status"] == "OK":
				driving_time = directions["routes"][0]["legs"][0]["duration"]["value"]/60
				if driving_time <= initial_driving_time + max_deviation_time:
					urlsToSearch[coordsWithinBounds[i-1]] = self.coordsToCities[coordsWithinBounds[i-1]]
		return urlsToSearch

	def getBounds(self, originLngLat, destLngLat, min_dist, max_dist):
		slope = (destLngLat[1] - originLngLat[1]) / (destLngLat[0] - originLngLat[0])

		# opposite reciprical
		new_slope = -1 / slope

		new_lat_origin_min = originLngLat[1] + min_dist * new_slope / math.sqrt(1 + math.pow(new_slope, 2))
		new_lat_origin_max = originLngLat[1] + max_dist * new_slope / math.sqrt(1 + math.pow(new_slope, 2))
		new_lat_dest_min = destLngLat[1] + min_dist * new_slope / math.sqrt(1 + math.pow(new_slope, 2))
		new_lat_dest_max = destLngLat[1] + max_dist * new_slope / math.sqrt(1 + math.pow(new_slope, 2))

		new_lng_origin_min = originLngLat[0] + (-originLngLat[1] + new_lat_origin_min) / new_slope
		new_lng_origin_max = originLngLat[0] + (new_lat_origin_max - originLngLat[1]) / new_slope
		new_lng_dest_min = destLngLat[0] + (-destLngLat[1] + new_lat_dest_min) / new_slope
		new_lng_dest_max = destLngLat[0] + (new_lat_dest_max - destLngLat[1]) / new_slope

		return (new_lng_origin_max, new_lat_origin_max),(new_lng_origin_min, new_lat_origin_min), (new_lng_dest_min, new_lat_dest_min), (new_lng_dest_max, new_lat_dest_max)
